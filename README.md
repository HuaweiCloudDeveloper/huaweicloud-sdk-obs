# 仓库简介

对象存储服务（Object Storage Service,OBS）的多语言版本sdk主仓库



# 语言总览

<table style="text-align: center">
    <tr style="font-weight: bold">
        <td>项目</td>
        <td>介绍</td>
        <td>仓库</td>
    </tr>  
    <tr>
        <td rowspan="1">c</td>
        <td>The OBS SDK for C, which is used for accessing Object Storage Service</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-sdk-c-obs">huaweicloud-sdk-c-obs</a></td>
    </tr>
    <tr>
        <td rowspan="1">go</td>
        <td>The OBS SDK for Go, which is used for accessing Object Storage Service</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-sdk-go-obs">huaweicloud-sdk-go-obs</a></td>
    </tr>
     <tr>
        <td rowspan="1">php</td>
        <td>The OBS SDK for php, which is used for accessing Object Storage Service</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-sdk-php-obs">huaweicloud-sdk-php-obs</a></td>
    </tr>
     <tr>
        <td rowspan="1">java</td>
        <td>The OBS SDK for Java, which is used for accessing Object Storage Service</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-sdk-java-obs">huaweicloud-sdk-java-obs</a></td>
    </tr>
     <tr>
        <td rowspan="1">python</td>
        <td>The OBS SDK for Python, which is used for accessing Object Storage Service</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-sdk-python-obs">huaweicloud-sdk-python-obs</a></td>
    </tr>
         <tr>
        <td rowspan="1">nodejs</td>
        <td>The OBS SDK for nodejs, which is used for accessing Object Storage Service</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-sdk-nodejs-obs"> huaweicloud-sdk-nodejs-obs</a></td>
    </tr>
         <tr>
        <td rowspan="1">dotnet</td>
        <td>The OBS SDK for dotnet, which is used for accessing Object Storage Service</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-sdk-dotnet-obs">huaweicloud-sdk-dotnet-obs</a></td>
    </tr>    
    <tr>
        <td rowspan="1">browserjs</td>
        <td>The OBS SDK for browserjs, which is used for accessing Object Storage Service</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-sdk-browserjs-obs">huaweicloud-sdk-browserjs-obs </a></td>
    </tr>

